/* arm.c
 * Martin Rodriguez
 * ECE 586 HW4 
 *
 * Accepts a 32-bit unsigned decimal (or hex if preceded with "0x") integer from 
 * command line argument and prints whether it can be represented (Legal/Not legal) 
 * using an ARM processor's immediate value encoding technique (4-bit rotate field 
 * and 8-bit immediate field). It then prints the value of the 4-bit rotate field 
 * in decimal and the value of the 8-bit immediate field in hex and in binary.
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define NBITS 32    // Number of bits in immediate address
#define IMMBITS 8   // Number of bits for ARM immediate field
#define ROTBITS 4   // Number of bits for ARM rotation field

int hexToBin(int encode, char binStr[]);
char binStr[IMMBITS+1];

int main(int argc, char *argv[])
{
    uint32_t encode = strtoul(argv[1], NULL, 0);
    int rotate;
    int legalCount = 0;
    int illegalCount = 0;

    for (rotate = 0; rotate < NBITS; rotate += 2) {
        if (!(encode & ~0xFFU)) {
            printf("Legal\n");
            legalCount++;
            printf("4-bit rotate field in decimal: %d\n", rotate/2);
            printf("8-bit immediate field in hexadecimal: 0x%02X\n", encode);
            hexToBin(encode, binStr);
            printf("8-bit immediate field in binary: %s\n", binStr);
            break;
        } else {
            illegalCount++;
        }
        encode = (encode << 2) | (encode >> 30);
    }
    if (illegalCount == NBITS/2) {
        printf("Not Legal\n");
    }
    return 0;
}

/* Function: hexToBin
 * ------------------
 *  Creates a binary value from each hexadecimal digit input, concatenated together
 *
 *  encode: hex integer to convert to binary
 *  binStr: char array in which to store converted binary values 
 */

int hexToBin(int encode, char binStr[])
{
    int i = 0;
    char hexStr[(IMMBITS/4)+1];
    sprintf(hexStr, "%X", encode);
    while (hexStr[i]) {
        switch (hexStr[i]) {
        case '0':
            strcat(binStr, "0000");
            break;
        case '1':
            strcat(binStr, "0001");
            break;
        case '2':
            strcat(binStr, "0010");
            break;
        case '3':
            strcat(binStr, "0011");
            break;
        case '4':
            strcat(binStr, "0100");
            break;
        case '5':
            strcat(binStr, "0101");
            break;
        case '6':
            strcat(binStr, "0110");
            break;
        case '7':
            strcat(binStr, "0111");
            break;
        case '8':
            strcat(binStr, "1000");
            break;
        case '9':
            strcat(binStr, "1001");
            break;
        case 'A':
        case 'a':
            strcat(binStr, "1010");
            break;
        case 'B':
        case 'b':
            strcat(binStr, "1011");
            break;
        case 'C':
        case 'c':
            strcat(binStr, "1100");
            break;
        case 'D':
        case 'd':
            strcat(binStr, "1101");
            break;
        case 'E':
        case 'e':
            strcat(binStr, "1110");
            break;
        case 'F':
        case 'f':
            strcat(binStr, "1111");
            break;
        }
        i++;
    }
    return 0;
}
